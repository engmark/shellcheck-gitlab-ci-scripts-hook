#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

for file in "${script_dir}/tests/valid/"*; do
    pre-commit try-repo --files="$file" --verbose . shellcheck-gitlab-ci-scripts
done

for file in "${script_dir}/tests/invalid/"*; do
    if pre-commit try-repo --files="$file" --verbose . shellcheck-gitlab-ci-scripts; then
        exit 1
    fi
done
