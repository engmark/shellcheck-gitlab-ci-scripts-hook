#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail
shopt -s failglob inherit_errexit

for file; do
    while IFS= read -r -u3 selector; do
        # The script can be either a list or a string.
        # To handle both, we first try to extract it as a list.
        # This results in an empty output if the script is a string, in which case we fall back to the original selector.
        script="$(yq "${selector}[]" "$file")"
        if [[ -z "$script" ]]; then
            script="$(yq "$selector" "$file")"
        fi

        if ! printf '%s' "$script" | shellcheck --shell=bash -; then
            printf "Script in %q with selector %s failed ShellCheck\n" "$file" "$selector" >&2
            exit_code=1
        fi
    done 3< <(yq '.[] | select(tag=="!!map") | (.before_script, .script, .after_script) | select(. != null ) | path | ".[\"" + join("\"].[\"") + "\"]"' "$file")
done

exit "${exit_code-0}"
