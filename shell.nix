let
  pkgs = import (builtins.fetchTarball {
    name = "nixos-unstable-2024-11-27";
    url = "https://github.com/nixos/nixpkgs/archive/4633a7c72337ea8fd23a4f2ba3972865e3ec685d.tar.gz";
    sha256 = "0z9jlamk8krq097a375qqhyj7ljzb6nlqh652rl4s00p2mf60f6r";
  }) { };
in
pkgs.mkShell {
  packages = [
    pkgs.bashInteractive
    pkgs.check-jsonschema
    pkgs.gitFull
    pkgs.gitlint
    pkgs.nixfmt-rfc-style
    pkgs.nodePackages.prettier
    pkgs.pre-commit
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.yq-go
  ];
}
