# ShellCheck GitLab CI scripts pre-commit hook

Run ShellCheck on the scripts in your GitLab CI configuration manually or using
[`pre-commit`](https://pre-commit.com/).

Prerequisites:

-  [shellcheck](https://www.shellcheck.net/)
-  [yq](https://github.com/mikefarah/yq)

## Use as pre-commit hook

To check the standard file, `.gitlab-ci.yml`:

<!-- prettier-ignore-start -->
```yaml
repos:
  - repo: https://gitlab.com/engmark/shellcheck-gitlab-ci-scripts-hook
    rev: REVISION # Replace either manually or with `pre-commit autoupdate --freeze`
    hooks:
      - id: shellcheck-gitlab-ci-scripts
        files: ^\.gitlab-ci\.yml$
```
<!-- prettier-ignore-end -->

## Stand-alone use

```bash
./shellcheck-gitlab-ci-scripts.bash FILE…
```
