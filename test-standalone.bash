#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

for file in "${script_dir}/tests/valid/"*; do
    "${script_dir}/shellcheck-gitlab-ci-scripts.bash" "$file"
done

for file in "${script_dir}/tests/invalid/"*; do
    if "${script_dir}/shellcheck-gitlab-ci-scripts.bash" "$file"; then
        exit 1
    fi
done
